## JS-12 - React example project - Webpack and Babel custom configuration example
This is an example React project covering multiple React features. Based on UDEMY course: https://www.udemy.com/react-the-complete-guide-incl-redux/.

**Examples in this project are focused mainly on creation of custom webpack and babel project configuration. This is basically done by command "create-react-app".**

#### Used commands and libs:
* npm init
* npm install --save-dev webpack webpack-dev-server
* npm install --save react react-dom react-router-dom
* npm install --save-dev babel-loader babel-core babel-preset-react babel-preset-env
* npm install --save-dev css-loader style-loader
* npm install --save-dev postcss-loader
* npm install --save-dev autoprefixer
* npm install --save-dev url-loader
* npm install --save-dev file-loader
* npm install --save-dev babel-plugin-syntax-dynamic-import
* npm install --save-dev babel-preset-stage-2
* npm install --save-dev html-webpack-plugin
* npm install --save-dev rimraf

#### Babel:
* *Babel* - standard for transforming next generation JS to standard JS
* https://github.com/browserslist/browserslist
